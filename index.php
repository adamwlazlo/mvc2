<?php
session_start();

require('config.php');

$bootstrap = new Bootstrap(substr($_SERVER['REQUEST_URI'],8));
$controller = $bootstrap->createController();
if($controller)
{
    $controller->executeAction();
}